




let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake sass"
];

console.log(myTasks);

// If you can shorten the array by setting the length of property, lengtthen it by adding number into the length property.


let theBeatles = ["John", "Paul", "Ringo", "George"];

theBeatles.length = theBeatles.length + 1;
console.log(theBeatles);


let lakersLegends =["Kobe","Shaq","Le Bron","Magic","Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[4]);


let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before the reassignment: ");
console.log(lakersLegends);

lakersLegends[2] = "Pau Gasol";
console.log("Array after the reassignment: ");
console.log(lakersLegends);

// Accessing the last element of an array
// Since the first element of an array start at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

let lastElementIndex = lakersLegends.length-1;

console.log(lakersLegends[lastElementIndex]);

// Adding items into the array without using array methods.

let newArr= [];
console.log(newArr);
newArr[0] = "Cloud Strife";
console.log(newArr);
newArr[newArr.length] = "Tifa Lockhart";

console.log(newArr);


for (let index =0; index < newArr.length; index++){
    console.log(newArr[index]);
}

// We are going to create a for loop, wherein it will check whether the elements/numbers are divisible by 5. 
let numArr = [5,12,30,46,40];

for (let index=0; index<numArr.length; index++){
    if(numArr[index]%5===0){
        console.log(numArr[index] + " is divisible by 5.");
    }
    else{
        console.log(numArr[index]+ " is not divisible by 5");
    }
}